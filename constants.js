const DEBUG = true

const PUBLIC_PATH = './public'

const MIME_TYPES = {
  html:'text/html',
  css:'text/css',
  js:'application/javascript',
  json:'application/json',
  jpg:'image/jpeg',
  jpeg:'image/jpeg',
  png:'image/png',
  gif:'image/gif',
  svg:'image/svg+xml',
  txt:'text/plain'
}

module.exports = {
  DEBUG,
  MIME_TYPES,
  PUBLIC_PATH
}
