const { DEBUG, MIME_TYPES, PUBLIC_PATH } = require('../constants.js')
const { mainView } = require('../views')
const { getExt, hasKey, readFile } = require('../utils.js')

const getView = (req) =>
  new Promise((resolve, reject) => {
    switch(req.url) {
      case '/':
        resolve(mainView(req))
        break
      default:
        reject('NO VIEW FOR THIS ROUTE')
        break
    }
  })

const logX = (x) => {
  console.log(x)
  return x
}

const getContent = (req) =>
  hasKey(getExt(req.url), MIME_TYPES) ?
    readFile(`${PUBLIC_PATH}${req.url}`) :
    getView(req)

module.exports = (req, res) =>
  new Promise((resolve, reject) => {
    try {
      getContent(req)
        .then((content) => {
          const mimetype = MIME_TYPES[getExt(req.url)] || MIME_TYPES.html
          resolve({
            code: 200,
            header: { 'content-type':mimetype },
            body:content
          })
        })
        .catch((reason) => {
          let output = DEBUG ? reason.toString() : 'NOT FOUND'
          reject({
            code: 404,
            header: { 'content-type':MIME_TYPES.html },
            body:`<h1>${output}</h1>`
          })
        })
    } catch(reason) {
      let output = DEBUG ?
        reason.toString() : 'INTERNAL SERVER ERROR'
      reject({
        code: 500,
        header: { 'content-type':MIME_TYPES.html },
        body:`<h1>${output}</h1>`
      })
    }
  })
