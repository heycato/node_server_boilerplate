const get = require('./get.js')
const post = require('./post.js')
const { MIME_TYPES } = require('../constants.js')

const sendResponse = (res, reply) => {
  res.writeHead(reply.code, reply.header)
  res.end(reply.body)
}

module.exports = (req, res) => {
  let processRequest
  switch(req.method.toLowerCase()) {
    case 'get':
      processRequest = get(req, res)
      break
    case 'post':
      processRequest = post(req, res)
      break
    default:
      processRequest = Promise.reject({
        code: 405,
        header: { 'content-type':MIME_TYPES.json },
        body: JSON.stringify({ err:'METHOD NOT ALLOWED' })
      })
      break
  }
  processRequest
    .then(sendResponse.bind(null, res))
    .catch(sendResponse.bind(null, res))
}
