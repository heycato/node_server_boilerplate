const { DEBUG, MIME_TYPES } = require('../constants.js')

module.exports = (req, res) => 
  new Promise((resolve, reject) => {
    let reqBody = ''
    req.on('data', (chunk) => reqBody += chunk)
    req.on('end', () => {
      try {
        const body = JSON.parse(reqBody)
        resolve({
          code: 200,
          header: { 'content-type':MIME_TYPES.json },
          body:JSON.stringify({ data:body })
        })
      } catch(reason) {
        let output = DEBUG ?
          reason.toString() : 'INTERNAL SERVER ERROR'
        reject({
          code: 500,
          header: { 'content-type':MIME_TYPES.json },
          body:JSON.stringify({ err:output })
        })
      }
    })
  })
