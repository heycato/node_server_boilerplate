const http = require(`http`)
const port = 8080
const router = require('./routes')

http
  .createServer(router)
  .listen(port, () => console.log(`Listening on port`, port))
