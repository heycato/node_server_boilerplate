const fs = require('fs')
const readFile = (path) =>
  new Promise((resolve, reject) => {
    console.log('filepath:', path)
    fs.readFile(path, (err, file) => {
      console.log('readFile', err, file)
      if(err) reject(err)
      else resolve(file)
    })
  })

const getExt = (path) => path.split('.').pop()

const hasKey = (key, obj) => (key in obj)

module.exports = {
  getExt,
  hasKey,
  readFile
}
