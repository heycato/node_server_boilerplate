const mainView = () =>
`<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title></title>
	<!--<link rel="stylesheet" type="text/css" href="styles/inline.css">-->
</head>
<body>
  <div>main view</div>
	<script src='js/main.js'></script>
</body>
</html>`

module.exports = { mainView }
